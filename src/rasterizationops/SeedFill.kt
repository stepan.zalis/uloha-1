package rasterizationops

import rasterdata.interfaces.RasterImage

import java.util.function.Predicate

interface SeedFill<PixelType> {

    fun fill(
            image: RasterImage<PixelType>,
            c: Int, r: Int,
            value: PixelType,
            validPixel: Predicate<PixelType>
    ): RasterImage<PixelType>
}
