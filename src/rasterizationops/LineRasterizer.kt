package rasterizationops

import rasterdata.interfaces.RasterImage

/**
 * Created by stepanzalis on 06.12.17.
 */


// T is parameter here
interface LineRasterizer<T> {

    // here it is a argument
    /**
     * Represents a line rasterization algorithm.
     * The line coordinates are considered normalized to a [-1;1] square
     * with the image bottom left corner being at (-1;-1) and the top right at (1;1)
     * @param background background image
     * @param x1 the x coordinate of the start-point in [-1;1] range
     * @param y1 the y coordinate of the start-point in [-1;1] range
     * @param x2 the x coordinate of the end-point in [-1;1] range
     * @param y2 the y coordinate of the end-point in [-1;1] range
     * @param value the value all line pixels should have
     * @return a new image with pixels corresponding to the line having the given value
     */
    fun rasterize(background: RasterImage<T>, x1: Double, y1: Double, x2: Double, y2: Double, value: T): RasterImage<T>

}
