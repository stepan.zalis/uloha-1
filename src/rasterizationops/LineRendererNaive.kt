package rasterizationops

import io.vavr.collection.Stream
import rasterdata.interfaces.RasterImage

/**
 * Created by stepanzalis on 06.12.17.
 */

class LineRendererNaive<T> : LineRasterizer<T> {

    override fun rasterize(background: RasterImage<T>, x1: Double, y1: Double, x2: Double, y2: Double, value: T): RasterImage<T> {

        val ix1 = (x1 + 1) * background.width / 2
        val iy1 = (-y1 + 1) * background.height / 2
        val ix2 = (x2 + 1) * background.width / 2
        val iy2 = (-y2 + 1) * background.height / 2

        // coordinates as integers
        var c1 = ix1.toInt()
        var r1 = iy1.toInt()
        var c2 = ix2.toInt()
        var r2 = iy2.toInt()

        val dx: Double = (c2 - c1).toDouble()
        val dy: Double = (r2 - r1).toDouble()


        // DDA
        if (Math.abs(dy) <= Math.abs(dx)) {

            // swap if needed
            if (c2 < c1) {
                c2 = c1.also { c1 = c2 }
                r2 = r1.also { r1 = r2 }
            }

            val k = dy / dx
            var y = r1.toDouble()

            return Stream.rangeClosed(c1, c2).foldLeft(background,
                    { currentImage, c ->
                        val intY = Math.round(y).toInt()
                        y += k
                        currentImage.withPixel(c, intY, value)
                    })
        } else {

            // swap if needed
            if (r2 < r1) {
                c2 = c1.also { c1 = c2 }
                r2 = r1.also { r1 = r2 }
            }

            val k = dx / dy
            var x = c1.toDouble()

            return Stream.rangeClosed(r1, r2).foldLeft(background,
                    { currentImage, r ->
                        val intX = Math.round(x).toInt()
                        x += k
                        currentImage.withPixel(intX, r, value)
                    }
            )

        }
    }
}









