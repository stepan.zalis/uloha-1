package models

import rasterdata.interfaces.RasterImage
import java.awt.Color
import java.awt.Point

/**
 * Created by stepanzalis on 23.12.17.
 */

class Circle {

    private var actualMode = Circle.Mode.CENTER
    private lateinit var center: Point
    private var lines: Lines = Lines()

    private var startD = 0.0
    private var endD = 0.0
    var lastPoint: Point? = null

    /**
     * @param rasterImage actual image
     * @param point point where is mouse cursor
     */
    fun draw(rasterImage: RasterImage<Int>, point: Point): RasterImage<Int> {

        lastPoint = point

        if (actualMode != Circle.Mode.SEGMENT) {
            startD = (point.x - center.getX())
            endD = (point.y - center.getY())
        }

        val r = Math.sqrt(Math.abs(startD * startD) + Math.abs(endD * endD))
        var fiStart = 0.0
        var fiEnd = Math.PI * 2
        val pointZeroRad = Point((center.getX() + r).toInt(), center.getY().toInt())

        if (actualMode == Circle.Mode.SEGMENT) {
            fiStart = Math.atan2(pointZeroRad.getY() - center.getY(), pointZeroRad.getX() - center.getX())

            if (fiStart < 0) {
                fiStart += Math.PI * 2
            }

            fiEnd = Math.atan2((point.getY() - center.getY()), (point.getX() - center.getX()))

            if (fiEnd < 0) {
                fiEnd += Math.PI * 2
            }

            if (fiEnd < fiStart) {
                fiEnd += Math.PI * 2
            }
            clearPoints()
        }

        var fi = fiStart

        while (fi <= fiEnd) {
            val x = r * Math.cos(fi)
            val y = r * Math.sin(fi)
            lines.addPoint(Point((center.getX() + x).toInt(), (center.getY() + y).toInt()))
            fi += 1 / r
        }

        return drawLine(rasterImage)
    }

    fun drawLine(rasterImage: RasterImage<Int>): RasterImage<Int> {

        var rasterImage = rasterImage

        var index = 0
        while (index < lines.getSize() - 2) {
            println("line:${lines.getPoint(index)}")
            val point1 = lines.getPoint(index)
            val point2 = lines.getPoint(index + 1)
            rasterImage = lines.draw(rasterImage, point1, point2, Color.BLACK.rgb)
            index += 1
        }
        return rasterImage
    }

    fun getCenter(): Point {
        return center
    }

    fun setCenter(newCenter: Point) {
        center = newCenter
    }

    fun clearPoints() {
        lines.clear()
    }

    fun setActualMode(mode: Mode) {
        actualMode = mode.getMode(mode.getValue() + 1)
    }

    fun getActualMode(): Circle.Mode {
        return actualMode
    }

    /**
     * @param mode Actual mode in int interpretation
     * This class is used to check the actual mode @see Canvas
     **/
    enum class Mode(private var mode: Int) {

        CENTER(1), RADIUS(2), SEGMENT(3);

        fun getValue(): Int {
            return mode
        }

        fun getMode(mode: Int): Mode {
            return when (mode) {
                1 -> CENTER
                2 -> RADIUS
                3 -> SEGMENT
                else -> CENTER
            }
        }
    }
}