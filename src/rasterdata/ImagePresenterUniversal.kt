package rasterdata

import io.vavr.collection.Stream
import rasterdata.interfaces.Presenter
import rasterdata.interfaces.RasterImage
import java.awt.Graphics
import java.awt.image.BufferedImage
import java.util.function.Function

/**
 * Created by stepanzalis on 29.11.17.
 */

open class ImagePresenterUniversal<PixelType>(private val toRGB : Function<PixelType, Int>) : Presenter<PixelType, Graphics> {


    /**
     * @return Graphics device
     */
    override fun present(img: RasterImage<PixelType>, device: Graphics): Graphics {

        val bufImg = BufferedImage(img.width, img.height, BufferedImage.TYPE_INT_ARGB)

        Stream.range(0, img.height).forEach { r ->
            Stream.range(0, img.width).forEach { c ->
                img.getPixel(c, r).ifPresent({ value -> bufImg.setRGB(c!!, r!!, toRGB.apply(value)) })
            }
        }

        device.drawImage(bufImg, 0, 0, null)

        return device
    }
}
