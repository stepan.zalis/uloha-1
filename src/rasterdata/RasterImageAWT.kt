package rasterdata

import rasterdata.interfaces.RasterImage
import java.awt.*
import java.awt.image.BufferedImage
import java.util.Optional
import java.util.function.Function

class RasterImageAWT<PixelType>(private val img: BufferedImage,
                                private val pixelType2Integer: Function<PixelType, Int>,
                                private val integer2PixelType: Function<Int, PixelType>) : RasterImage<PixelType> {

    override fun getPixel(c: Int, r: Int): Optional<PixelType> =
            if (0 <= c && c < img.width && 0 <= r && r < img.height)
                Optional.of(integer2PixelType.apply(img.getRGB(c, r)))
            else Optional.empty()


    override fun withPixel(c: Int, r: Int, value: PixelType): RasterImage<PixelType> {
        if (0 <= c && c < img.width && 0 <= r && r < img.height) {
            img.setRGB(c, r, pixelType2Integer.apply(value))
        }
        return this
    }

    override fun cleared(value: PixelType): RasterImage<PixelType> {
        val gr = img.graphics
        gr.color = Color(pixelType2Integer.apply(value))
        gr.fillRect(0, 0, img.width, img.height)
        return this
    }

    override fun getWidth(): Int = img.width

    override fun getHeight(): Int = img.height

    fun getBufferedImage() : BufferedImage = img

}

