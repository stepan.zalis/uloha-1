package rasterdata.interfaces

/**
 * Created by stepanzalis on 29.11.17.
 */

open interface Presenter<PixelType, DeviceType> {

    /**
     * Presents the given image to the given device
     *
     * @param img    image to be presented
     * @param device device to be presented into
     * @return New device reflecting the presented image
     */
    fun present(img: RasterImage<PixelType>, device: DeviceType): DeviceType
}
