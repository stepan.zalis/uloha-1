package rasterdata

import rasterdata.interfaces.Presenter
import rasterdata.interfaces.RasterImage

import java.awt.*
import java.awt.image.BufferedImage

/**
 * Created by stepanzalis on 29.11.17.
 */
open class ImagePresenterAWT<PixelType> : Presenter<PixelType, Graphics> {


    /**
     * @return device
     */
    override fun present(img: RasterImage<PixelType>, device: Graphics): Graphics {

        if (img is RasterImageAWT<*>) {
            val bufImg = img.getBufferedImage()
            device.drawImage(bufImg, 0, 0, null)
        } else {
            System.err.println("Cannot present image of type: " + img.javaClass)
        }

        return device
    }
}
